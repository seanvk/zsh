#
# Executes commands at login pre-zshrc.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#

#
# Browser
#

if [[ "$OSTYPE" == darwin* ]]; then
  export BROWSER='open'
fi

#
# Editors
#

export EDITOR='vim'
export VISUAL='vim'
export PAGER='less'

#
# Language
#

if [[ -z "$LANG" ]]; then
  export LANG='en_US.UTF-8'
fi

#
# Paths
#

# Ensure path arrays do not contain duplicates.
typeset -gU cdpath fpath mailpath path

# Set the list of directories that cd searches.
# cdpath=(
#   $cdpath
# )

# Set the list of directories that Zsh searches for programs.
path=(

  ${HOME}/bin
  ${HOME}/.local/bin
  /usr/local/{bin,sbin}
  /snap/bin
  $path
)

#
# Less
#

# Set the default Less options.
# Mouse-wheel scrolling has been disabled by -X (disable screen clearing).
# Remove -X and -F (exit if the content fits on one screen) to enable it.
export LESS='-F -g -i -M -R -S -w -X -z-4'

# Set the Less input preprocessor.
# Try both `lesspipe` and `lesspipe.sh` as either might exist on a system.
if (( $#commands[(i)lesspipe(|.sh)] )); then
  export LESSOPEN="| /usr/bin/env $commands[(i)lesspipe(|.sh)] %s 2>&-"
fi

# To speed up builds
export USE_CCACHE=1
export CCACHE_COMPILERCHECK=none
export CCACHE_SLOPPINESS=time_macros,include_file_mtime,file_macro
export CCACHE_DIR='/home/seanvk/.ccache/'

# Make sure pkg-config can find self-compiled software
# and libraries (installed to ~/usr)
if [ -n $PKG_CONFIG_PATH ]; then
  PKG_CONFIG_PATH=/usr/local/lib64/pkgconfig:/usr/lib64/pkgconfig:/usr/lib/pkgconfig:/usr/local/lib/pkgconfig:~/.local/usr/lib/pkgconfig
else
  PKG_CONFIG_PATH=$PKG_CONFIG_PATH:~/.local/usr/lib/pkgconfig
fi
export PKG_CONFIG_PATH

# Add custom compiled libraries to library search path.
if [ -n $LD_LIBRARY_PATH ]; then
  LD_LIBRARY_PATH=/usr/local/lib64:/usr/lib64:/usr/lib:/usr/local/lib:~/.local/usr/lib
else
  LD_LIBRARY_PATH=$LD_LIBRARY_PATH:~/.local/usr/lib
fi
export LD_LIBRARY_PATH

# Add custom compiled libraries to library run path.
if [ -n $LD_RUN_PATH ]; then
  LD_RUN_PATH=/usr/local/lib64:/usr/lib64:/usr/lib:/usr/local/lib:~/.local/usr/lib
else
  LD_RUN_PATH=$LD_RUN_PATH:~/usr/lib
fi
export LD_RUN_PATH

# Locale
export LANGUAGE="en_US:en"
export LC_ALL="en_US.UTF-8"
export LC_MESSAGES="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"
export LC_COLLATE="en_US.UTF-8"

# openssl bug 33919 work-around
#export  OPENSSL_NO_TLS1_2=1

# Python / Gyp 64bit
export GYP_DEFINES=system_libdir=lib64

# Winbind
export NTLMUSER=svkelley
